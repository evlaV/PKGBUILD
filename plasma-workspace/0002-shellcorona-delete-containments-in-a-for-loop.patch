From 96be099af0d70e7804eeafc1d9f7ceac78f5436a Mon Sep 17 00:00:00 2001
From: Marco Martin <notmart@gmail.com>
Date: Thu, 20 Feb 2025 13:30:57 +0000
Subject: [PATCH] shellcorona: delete containments in a for loop

Backport from 6.3 to 6.2. This patch can be dropped after 6.3 upgrade
as there is always the possibility destroy is a no-op, iterate
over containments with a for loop rather than a while

CCBUG:498175
---
 shell/shellcorona.cpp | 12 ++++++++++--
 1 file changed, 10 insertions(+), 2 deletions(-)

diff --git a/shell/shellcorona.cpp b/shell/shellcorona.cpp
index 127f5da10c..841eb08529 100644
--- a/shell/shellcorona.cpp
+++ b/shell/shellcorona.cpp
@@ -51,6 +51,7 @@
 #include <qassert.h>
 
 #include "alternativeshelper.h"
+#include "containmentconfigview.h"
 #include "debug.h"
 #include "desktopview.h"
 #include "futureutil.h"
@@ -990,13 +991,20 @@ void ShellCorona::unload()
     m_waitingPanels.clear();
     m_activityContainmentPlugins.clear();
 
-    while (!containments().isEmpty()) {
+    // iterate with a for on a copy of the list making sure this loop will end
+    // (destroy() might be a noop in case of immutability)
+    const QList<Plasma::Containment *> conts = containments();
+    for (Plasma::Containment *cont : conts) {
         // Some applets react to destroyedChanged rather just destroyed,
         // give them  the possibility to react
         // deleting a containment will remove it from the list due to QObject::destroyed connect in Corona
         // this form doesn't crash, while qDeleteAll(containments()) does
         // And is more correct anyways to use destroy()
-        containments().constFirst()->destroy();
+        // Exclude CustomEmbedded containments like the systray as they
+        // will already be deleted by their parent applet
+        if (cont->containmentType() != Plasma::Containment::CustomEmbedded) {
+            cont->destroy();
+        }
     }
 }
 
-- 
2.43.0

